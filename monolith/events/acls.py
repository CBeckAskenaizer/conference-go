from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    # Create the URL for the request with the city and state
    url = "https://api.pexels.com/v1/search"

    params = {"per_page": 1, "query": city + " " + state}

    headers = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, params=params, headers=headers)
    # ?query=nature
    # Make the request
    # Parse the JSON response
    content = json.loads(response.content)
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError):
        return {"picture_url": None}


def get_weather_data(city, state):
    pass
    # Create the URL for the geocoding API with the city and state
    geocode_url = f"http://api.openweathermap.org/geo/1.0/direct"

    geo_params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }

    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    # Make the request
    response = requests.get(geocode_url, params=geo_params, headers=headers)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the latitude and longitude from the response
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None
    # Create the URL for the current weather API with the latitude
    #   and longitude
    weather_url = "https://api.openweathermap.org/data/2.5/weather"

    headers = {"Authorization": OPEN_WEATHER_API_KEY}

    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }

    # Make the request
    response = requests.get(
        weather_url, params=weather_params, headers=headers
    )
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    try:
        weather = {
            "temperature": content["main"]["temp"],
            "weather_description": content["weather"][0]["description"],
        }
        return weather
    except (KeyError, IndexError):
        return None


# def api_list_locations(request):
#     """
#     Lists the location names and the link to the location.

#     Returns a dictionary with a single key "locations" which
#     is a list of location names and URLS. Each entry in the list
#     is a dictionary that contains the name of the location and
#     the link to the location's information.

#     {
#         "locations": [
#             {
#                 "name": location's name,
#                 "href": URL to the location,
#             },
#             ...
#         ]
#     }
#     """
#     if request.method == "GET":
#         locations = Location.objects.all()
#         return JsonResponse(
#             {"locations": locations}, encoder=LocationListEncoder
#         )
#     else:
#         content = json.loads(request.body)

#         try:
#             if "state" in content:
#                 state = State.objects.get(abbreviation=content["state"])
#                 content["state"] = state
#         except State.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid state abbreviation"},
#                 status=400,
#             )
#         photo = get_photo(content["city"], content["state"].abbreviation)
#         content.update(photo)

#         location = Location.objects.create(**content)
#         return JsonResponse(
#             location,
#             encoder=LocationDetailEncoder,
#             safe=False,
#         )
